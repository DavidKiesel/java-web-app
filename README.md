java-web-app
============

# Introduction

Simple Java web app.

# Maven

## `javadoc:javadoc`

The command below does the following:

- clean
- build javadoc files

```bash
mvn \
    clean \
    javadoc:javadoc
```

## `test`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute unit tests tests

```bash
mvn \
    clean \
    test
```

## `cobertura:cobertura`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute unit tests tests
- generate a Cobertura test coverage report

```bash
mvn \
    clean \
    cobertura:cobertura
```

## `package`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file

```bash
mvn \
    clean \
    package
```

## `jetty:run`

The command below does the following:

- clean
- compile classes
- executes web application in a Jetty container
- http://localhost:8080/
- http://localhost:8080/get_listener
- http://localhost:8080/post_listener

```bash
mvn \
    clean \
    jetty:run
```

## `install`

The command below does the following:

- clean
- compile classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- create checksums on artifacts
- gpg sign artifacts
- install the artifacts in the local repository

```bash
mvn \
    clean \
    install
```

## `deploy`

The command below does the following:

- clean
- compile classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- create checksums on artifacts
- gpg sign artifacts
- install the artifact in the local repository
- deploy the artifacts to the libs-snapshot repository of artifactory (assuming element project.version in pom.xml is
  something like 1.2.3-SNAPSHOT)

```bash
git checkout master

mvn \
    clean \
    deploy
```

## `release:prepare`

The command below does the following:

- clean
- change pom.xml project.version version to release version (e.g., from 1.2.3-SNAPSHOT TO 1.2.3)
- change pom.xml project.scm.tag version to release version (e.g., from master to v1.2.3)
- compile classes
- execute tests
- build the jar file
- gpg sign artifacts
- commit modified POM(s)
- tag latest commit in SCM with release version name
- change pom.xml project.version version to next SNAPSHOT version (e.g., from 1.2.3 to 1.2.4-SNAPSHOT)
- change pom.xml project.scm.tag version to prior value (e.g., from v1.2.3 to master)
- commit modified POM(s)
- push commits to remote

```bash
git checkout master

mvn \
    -DscmCommentPrefix='My comment line one.${line.separator}My comment line two.${line.separator}${line.separator}' \
    clean \
    release:prepare
```

Or something like...

```bash
git checkout master

mvn \
    -DscmCommentPrefix="$(cat tmp/release_comments/v1.0.1.txt)"'${line.separator}${line.separator}' \
    clean \
    release:prepare
```

References:

- [http://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html](http://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html)

## `release:perform`

- checkout code with release tag given by prior release:prepare run into directory workingDirectory (default
  ${project.build.directory}/checkout)
- within directory workingDirectory, execute Maven release goals (default deploy site-deploy)
    - compile classes
    - execute tests
    - build the jar file
    - create source jar file
    - create javadoc jar file
    - create checksums on artifacts
    - gpg sign artifacts
    - install the artifact in the local repository
    - deploy the artifacts to the libs-release repository of artifactory

```bash
mvn \
    release:perform
```

References:

[http://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html](http://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html)

## `site`

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- generate site javadoc documentation
- generate site cobertura coverage report

```bash
mvn \
    clean \
    site
```