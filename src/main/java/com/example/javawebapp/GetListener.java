package com.example.javawebapp;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * <p>Java web servlet; listens for GET.</p>
 */
@WebServlet(
        value = "/get_listener"
)
public class GetListener
        extends HttpServlet
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            GetListener.class
    );

    //////////////////////////////////////////////////////////////////////////
    // instance fields

    List<String> appStringList;

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>Handle GET method.</p>
     *
     * @param request  HttpServletRequest.
     * @param response  HttpServletResponse.
     *
     * @throws IOException  Thrown if {@code response.getWriter()} fails.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void doGet(
            HttpServletRequest request,
            HttpServletResponse response
    )
            throws IOException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin doGet(HttpServletRequest,HttpServletResponse)");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("set HTTP content type");

        response.setContentType("text/plain");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("get servletContext parameters");

        final ServletContext servletContext = request.getServletContext();

        appStringList =
                (List<String>)
                        servletContext
                                .getAttribute("appStringList")
        ;

        LOGGER.debug(
                "appStringList: ",
                appStringList
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("get request parameters");

        final String foo = request.getParameter("foo");

        LOGGER.debug(
                "foo: ",
                foo
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("processRequest");

        String processResponse;

        try
        {
            processResponse = processRequest(
                    foo
            );
        }
        catch (IllegalArgumentException exception)
        {
            String stackTrace = ExceptionUtils.getStackTrace(
                    exception
            );

            LOGGER.error(stackTrace);

            response.getWriter().println(stackTrace);

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            return;
        }

        LOGGER.debug(
                "processResponse: ",
                processResponse
        );

        response
                .getWriter()
                .println(processResponse)
        ;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("set HTTP status OK");

        response.setStatus(
                HttpServletResponse.SC_OK
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end doGet(HttpServletRequest,HttpServletResponse)");
    }

    /**
     * <p>Process the request.</p>
     *
     * @param foo  Parameter foo.
     *
     * @return processResponse  The process response.
     *
     * @throws IllegalArgumentException  If invalid argument.
     */
    String processRequest(
            String foo
    )
        throws IllegalArgumentException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest(String)");

        String processResponse =
                String.join(
                        "\n",
                        appStringList
                );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("check foo");

        if (foo == null)
        {
            return processResponse;
        }

        if (
                foo.equals("badfoo")
                )
        {
            throw new IllegalArgumentException(
                    "foo should not be set to badfoo."
            );
        }

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("compute processResponse");

        processResponse =
                processResponse
                        + "\n"
                        + new StringBuilder(foo)
                        .reverse()
                        .toString()
        ;

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest(String)");

        return processResponse;
    }
}