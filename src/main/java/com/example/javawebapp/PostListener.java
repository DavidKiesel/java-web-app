package com.example.javawebapp;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <p>Java web servlet; listens for POST.</p>
 */
@WebServlet(
        value = "/post_listener"
)
public class PostListener
        extends HttpServlet
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            PostListener.class
    );

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>Handle POST method.</p>
     *
     * @param httpServletRequest  HttpServletRequest.
     * @param httpServletResponse  HttpServletResponse.
     *
     * @throws IOException  Thrown if {@code httpServletResponse.getWriter()} fails.
     */
    @Override
    public void doPost(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse
    )
            throws IOException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin doPost(httpServletRequest,httpServletResponse)");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("set HTTP content type");

        httpServletResponse.setContentType("text/plain");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("get requestBody");

        String requestBody =
                httpServletRequest
                        .getReader()
                        .lines()
                        .collect(
                                Collectors
                                        .joining("\n")
                        );

        LOGGER.debug(
                "requestBody: {}",
                requestBody
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("processRequest");

        String processResponse = processRequest(requestBody);

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        httpServletResponse
                .getWriter()
                .println(processResponse)
        ;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("set HTTP status OK");

        httpServletResponse.setStatus(
                HttpServletResponse.SC_OK
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end doPost(httpServletRequest,httpServletResponse)");
    }

    /**
     * <p>Process the request.</p>
     *
     * @param requestBody  The request body.
     *
     * @return processResponse  The process response.
     */
    String processRequest(
            String requestBody
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest(String)");

        String processResponse =
                new StringBuilder(requestBody)
                        .reverse()
                        .toString()
                ;

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest(String)");

        return processResponse;
    }
}