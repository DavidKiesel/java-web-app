package com.example.javawebapp;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * <p>
 *      Class for receiving notification events about ServletContext lifecycle
 *      changes.
 * </p>
 */
@WebListener
public class AppServletContextListener
    implements ServletContextListener
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            AppServletContextListener.class
    );

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>
     *     Receives notification that the web application initialization
     *     process is starting.
     * </p>
     *
     * <p>
     *     All ServletContextListeners are notified of context initialization
     *     before any filters or servlets in the web application are
     *     initialized.
     * </p>
     *
     * <p>
     *     Global web application variables (e.g., a database connection) can
     *     be initialized here because this code is guaranteed to be executed
     *     once during the execution of the web application and it executes
     *     prior to execution of any filters or servlets.
     * </p>
     *
     * @param servletContextEvent  provides ServletContext
     */
    @Override
    public void contextInitialized(
            ServletContextEvent servletContextEvent
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin contextInitialized(ServletContextEvent)");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("set appStringList");

        final ServletContext servletContext = servletContextEvent.getServletContext();
        
        final List<String> appStringList = Arrays.asList(
                "appStringList-1",
                "appStringList-2",
                "appStringList-3"
        );

        LOGGER.debug(
                "appStringList: {}",
                appStringList
        );

        servletContext.setAttribute(
                "appStringList",
                appStringList
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end contextInitialized(ServletContextEvent)");
    }

    /**
     * <p>
     *     Receives notification that the ServletContext is about to be shut
     *     down.
     * </p>
     *
     * <p>
     *     All servlets and filters will have been destroyed before any
     *     ServletContextListeners are notified of context destruction.
     * </p>
     *
     * <p>
     *     Global web application variables (e.g., a database connection) can
     *     be closed/destroyed here because this code is guaranteed to be
     *     executed once during the execution of the web application and it
     *     executes after destruction of any filters or servlets.
     * </p>
     *
     * @param servletContextEvent  provides ServletContext
     */
    @Override
    public void contextDestroyed(
            ServletContextEvent servletContextEvent
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin contextDestroyed(ServletContextEvent)");
        
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end contextDestroyed(ServletContextEvent)");
    }
}