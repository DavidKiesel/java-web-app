package com.example.javawebapp;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * GetListener unit tests.
 *
 * test method naming convention:
 *
 * productionMethod_stateUnderTest_expectedBehavior
 */
@Tag("parallel")
class GetListenerTest
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            GetListenerTest.class
    );

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link GetListener#doGet(HttpServletRequest, HttpServletResponse)} </p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     *
     * @throws IOException  From {@link GetListener#doGet(HttpServletRequest, HttpServletResponse)}.
     */
    @Test
    void doGet_normal_normal()
            throws IOException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin doGet_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        // create new instance of service to test
        GetListener getListener = new GetListener();

        // create and configure ServletContext mock

        ServletContext servletContext = mock(
                ServletContext.class
        );

        when(
                servletContext
                        .getAttribute("appStringList")
        )
                .thenReturn(
                        Arrays.asList(
                                "appStringList-1",
                                "appStringList-2",
                                "appStringList-3"
                        )
                )
        ;

        // create and configure HttpServletRequest mock

        HttpServletRequest httpServletRequest = mock(
                HttpServletRequest.class
        );

        when(
                httpServletRequest
                        .getServletContext()
        )
                .thenReturn(
                        servletContext
                )
        ;

        when(
                httpServletRequest
                        .getParameter("foo")
        )
                .thenReturn(
                        "abcdefg"
                )
        ;

        // create and configure HttpServletResponse mock

        HttpServletResponse httpServletResponse = mock(
                HttpServletResponse.class
        );

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PrintWriter printWriter = new PrintWriter(
                byteArrayOutputStream
        );

        when(
                httpServletResponse
                        .getWriter()
        )
                .thenReturn(
                        printWriter
                )
        ;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        // execute doPost of service to test
        getListener.doGet(
                httpServletRequest,
                httpServletResponse
        );

        // need to flush printWriter to guarantee bytes arrive in byteArrayOutputStream
        printWriter.flush();

        byte[] responseBody = byteArrayOutputStream.toByteArray();

        LOGGER.debug(
                "responseBody.length: {}",
                responseBody.length
        );

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug(
                    "responseBody as String: {}",
                    new String(
                            responseBody,
                            StandardCharsets.UTF_8
                    )
            );
        }

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertArrayEquals(
                "appStringList-1\nappStringList-2\nappStringList-3\ngfedcba\n".getBytes(StandardCharsets.UTF_8),
                byteArrayOutputStream.toByteArray(),
                "responseBody should be appStringList plus newline plus reverse of foo plus newline."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end doGet_normal_normal()");
    }

    /**
     * <p>method: {@link GetListener#doGet(HttpServletRequest, HttpServletResponse)} </p>
     *
     * <p>state: invalid parameter foo</p>
     *
     * <p>expected: SC_BAD_REQUEST</p>
     *
     * @throws IOException  From {@link GetListener#doGet(HttpServletRequest, HttpServletResponse)}.
     */
    @Test
    void doGet_badfoo_SC_BAD_REQUEST()
            throws IOException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin doGet_badfoo_SC_BAD_REQUEST()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        // create new instance of service to test
        GetListener getListener = new GetListener();

        // create and configure ServletContext mock

        ServletContext servletContext = mock(
                ServletContext.class
        );

        when(
                servletContext
                        .getAttribute("appStringList")
        )
                .thenReturn(
                        Arrays.asList(
                                "appStringList-1",
                                "appStringList-2",
                                "appStringList-3"
                        )
                )
        ;

        // create and configure HttpServletRequest mock

        HttpServletRequest httpServletRequest = mock(
                HttpServletRequest.class
        );

        when(
                httpServletRequest
                        .getServletContext()
        )
                .thenReturn(
                        servletContext
                )
        ;

        when(
                httpServletRequest
                        .getParameter("foo")
        )
                .thenReturn(
                        "badfoo"
                )
        ;

        // create and configure HttpServletResponse mock

        HttpServletResponse httpServletResponse = mock(
                HttpServletResponse.class
        );

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PrintWriter printWriter = new PrintWriter(
                byteArrayOutputStream
        );

        when(
                httpServletResponse
                        .getWriter()
        )
                .thenReturn(
                        printWriter
                )
        ;

        ArgumentCaptor<Integer> status =
                ArgumentCaptor
                        .forClass(
                                Integer.class
                        )
                ;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        // execute doPost of service to test
        getListener.doGet(
                httpServletRequest,
                httpServletResponse
        );

        // need to flush printWriter to guarantee bytes arrive in byteArrayOutputStream
        printWriter.flush();

        byte[] responseBody = byteArrayOutputStream.toByteArray();

        LOGGER.debug(
                "responseBody.length: {}",
                responseBody.length
        );

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug(
                    "responseBody as String: {}",
                    new String(
                            responseBody,
                            StandardCharsets.UTF_8
                    )
            );
        }

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        String expectedResponseBody =
                "java.lang.IllegalArgumentException: foo should not be set to badfoo.\n";

        Assertions.assertArrayEquals(
                expectedResponseBody
                        .getBytes(
                                StandardCharsets.UTF_8
                        ),
                Arrays.copyOfRange(
                        byteArrayOutputStream
                                .toByteArray(),
                        0,
                        expectedResponseBody
                                .length()
                ),
                "responseBody should contain IllegalArgumentException message."
        );

        verify(
                httpServletResponse
        )
                .setStatus(
                        status.capture()
                )
        ;

        Assertions.assertEquals(
                HttpServletResponse.SC_BAD_REQUEST,
                status.getValue().intValue(),
                "status should be SC_BAD_REQUEST."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end doGet_badfoo_SC_BAD_REQUEST()");
    }

    /**
     * <p>method: {@link GetListener#processRequest(String)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void processRequest_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        GetListener getListener = new GetListener();

        getListener.appStringList =
                Arrays.asList(
                        "appStringList-1",
                        "appStringList-2",
                        "appStringList-3"
                );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        String processResponse = getListener.processRequest(
                "abcdefg"
        );

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "appStringList-1\nappStringList-2\nappStringList-3\ngfedcba",
                processResponse,
                "processResponse should be appStringList plus newline plus reverse of foo."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest_normal_normal()");
    }

    /**
     * <p>method: {@link GetListener#processRequest(String)}</p>
     *
     * <p>state: foo = null</p>
     *
     * <p>expected: empty string</p>
     */
    @Test
    void processRequest_null_emptyString()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest_null_emptyString()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        GetListener getListener = new GetListener();

        getListener.appStringList =
                Arrays.asList(
                        "appStringList-1",
                        "appStringList-2",
                        "appStringList-3"
                );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        String processResponse = getListener.processRequest(
                null
        );

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "appStringList-1\nappStringList-2\nappStringList-3",
                processResponse,
                "processResponse for foo = null should be appStringList."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest_null_emptyString()");
    }

    /**
     * <p>method: {@link GetListener#processRequest(String)}</p>
     *
     * <p>state: parameter foo = badfoo</p>
     *
     * <p>expected: IllegalArgumentException</p>
     */
    @Test
    void processRequest_badfoo_IllegalArgumentException()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest_badfoo_IllegalArgumentException()");

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () ->
                {
                    //////////////////////////////////////////////////////////
                    LOGGER.trace("setup");

                    GetListener getListener = new GetListener();

                    getListener.appStringList =
                            Arrays.asList(
                                    "appStringList-1",
                                    "appStringList-2",
                                    "appStringList-3"
                            );

                    //////////////////////////////////////////////////////////
                    LOGGER.trace("process");

                    String processResponse = getListener.processRequest(
                            "badfoo"
                    );

                    LOGGER.debug(
                            "processResponse: {}",
                            processResponse
                    );

                    //////////////////////////////////////////////////////////
                    LOGGER.trace("assertions");
                },
                "If foo = badfoo, then processResponse throws IllegalArgumentException."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest_badfoo_IllegalArgumentException()");
    }
}