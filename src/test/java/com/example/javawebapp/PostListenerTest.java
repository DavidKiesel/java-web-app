package com.example.javawebapp;

import java.io.*;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * PostListener unit tests.
 *
 * test method naming convention:
 *
 * productionMethod_stateUnderTest_expectedBehavior
 */
@Tag("parallel")
class PostListenerTest
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            PostListenerTest.class
    );

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link PostListener#doPost(HttpServletRequest, HttpServletResponse)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     *
     * @throws IOException  From {@link PostListener#doPost(HttpServletRequest, HttpServletResponse)}.
     */
    @Test
    void doPost_normal_normal()
            throws IOException
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin doPost_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        // create new instance of service to test
        PostListener postListener = new PostListener();

        // create and configure HttpServletRequest mock

        HttpServletRequest httpServletRequest = mock(
                HttpServletRequest.class
        );

        when(
                httpServletRequest
                        .getReader()
        )
                .thenReturn(
                        new BufferedReader(
                                new InputStreamReader(
                                        new ByteArrayInputStream(
                                                "abcdefg"
                                                        .getBytes(StandardCharsets.UTF_8)
                                        )
                                )
                        )
                )
        ;

        // create and configure HttpServletResponse mock

        HttpServletResponse httpServletResponse = mock(
                HttpServletResponse.class
        );

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PrintWriter printWriter = new PrintWriter(
                byteArrayOutputStream
        );

        when(
                httpServletResponse
                        .getWriter()
        )
                .thenReturn(
                        printWriter
                )
        ;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        // execute doPost of service to test
        postListener.doPost(
                httpServletRequest,
                httpServletResponse
        );

        // need to flush printWriter to guarantee bytes arrive in byteArrayOutputStream
        printWriter.flush();

        byte[] responseBody = byteArrayOutputStream.toByteArray();

        LOGGER.debug(
                "responseBody.length: {}",
                responseBody.length
        );

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug(
                    "responseBody as String: {}",
                    new String(
                            responseBody,
                            StandardCharsets.UTF_8
                    )
            );
        }

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertArrayEquals(
                "gfedcba\n".getBytes(StandardCharsets.UTF_8),
                byteArrayOutputStream.toByteArray(),
                "responseBody should be reverse of requestBody."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end doPost_normal_normal()");
    }

    /**
     * <p>method: {@link PostListener#processRequest(String)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void processRequest_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processRequest_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        PostListener postListener = new PostListener();

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        String processResponse = postListener.processRequest(
                "abcdefg"
        );

        LOGGER.debug(
                "processResponse: {}",
                processResponse
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertEquals(
                "gfedcba",
                processResponse,
                "processResponse should be reverse of requestBody."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processRequest_normal_normal()");
    }
}