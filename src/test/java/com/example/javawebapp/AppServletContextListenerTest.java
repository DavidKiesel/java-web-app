package com.example.javawebapp;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;

/**
 * AppServletContextListener unit tests.
 *
 * test method naming convention:
 *
 * productionMethod_stateUnderTest_expectedBehavior
 */
@Tag("parallel")
class AppServletContextListenerTest
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER = LogManager.getLogger(
            AppServletContextListenerTest.class
    );

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link AppServletContextListener#contextInitialized(ServletContextEvent)} </p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void contextInitialized_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin contextInitialized_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        // create new instance to test
        AppServletContextListener appServletContextListener = new AppServletContextListener();

        // create and configure ServletContext mock

        ServletContext servletContext = mock(
                ServletContext.class
        );

        // create and configure ServletContext mock

        ServletContextEvent servletContextEvent = mock(
                ServletContextEvent.class
        );

        when(
                servletContextEvent
                .getServletContext()
        )
                .thenReturn(
                        servletContext
                );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        // execute doPost of service to test
        appServletContextListener.contextInitialized(
                servletContextEvent
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        ArgumentCaptor<String> attributeName =
                ArgumentCaptor.forClass(
                        String.class
                );

        @SuppressWarnings("unchecked")
        ArgumentCaptor<List<String>> attributeValue =
                ArgumentCaptor.forClass(
                        List.class
                );

        verify(
                servletContext,
                times(1)
        )
                .setAttribute(
                        attributeName.capture(),
                        attributeValue.capture()
                )
        ;

        LOGGER.debug(
                "attributeName.getValue(): {}",
                attributeName.getValue()
        );

        LOGGER.debug(
                "attributeValue.getValue(): {}",
                attributeValue.getValue()
        );

        Assertions.assertEquals(
                "appStringList",
                attributeName.getValue(),
                "In contextInitialized, setAttribute should capture correct attributeName."
        );

        Assertions.assertEquals(
                Arrays
                        .asList(
                                "appStringList-1",
                                "appStringList-2",
                                "appStringList-3"
                        ),
                attributeValue.getValue(),
                "In contextInitialized, setAttribute should capture correct attributeValue.");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end contextInitialized_normal_normal()");
    }

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link AppServletContextListener#contextDestroyed(ServletContextEvent)} </p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void contextDestroyed_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin contextDestroyed_normal_normal()");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup");

        // create new instance to test
        AppServletContextListener appServletContextListener = new AppServletContextListener();

        // create and configure ServletContext mock

        ServletContext servletContext = mock(
                ServletContext.class
        );

        // create and configure ServletContext mock

        ServletContextEvent servletContextEvent = mock(
                ServletContextEvent.class
        );

        when(
                servletContextEvent
                        .getServletContext()
        )
                .thenReturn(
                        servletContext
                );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process");

        // execute doPost of service to test
        appServletContextListener.contextDestroyed(
                servletContextEvent
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions");

        Assertions.assertTrue(
                true,
                "Nothing to check."
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end contextDestroyed_normal_normal()");
    }
}